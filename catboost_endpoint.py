from catboost import CatBoostClassifier


class CatBoostWrapper:
    def __init__(self, file):
        self.model = CatBoostClassifier()  # parameters not required.
        self.model.load_model(file)

    def compute_metric(self, text: str):
        return self.model.predict([text])
