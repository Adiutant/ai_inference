import hashlib

import pandas as pd


def testing(csv_filename, application):
    df = pd.read_csv(csv_filename)
    df['questions'] = df[['question_2', 'question_3', 'question_4', 'question_5']].apply(
        lambda x: ' '.join(x.dropna()), axis=1)
    sentiment, object_label, relevant = application.test_compute_all_metrics(df["questions"])
    df["is_positive"] = sentiment
    df["object"] = object_label
    df["is_relevant"] = relevant
    df["hash"] = df["questions"].apply(lambda x: hashlib.md5(x.encode('utf-8')).hexdigest())
    df = df[["hash", "is_relevant", "object", "is_positive"]]
    df.to_csv("submission.csv", sep=",", encoding="utf-8", index=False)
