import base64
import io
import os

from torch import cuda

import testing
from app import app
from flask import request
import threading
from flask import jsonify

from pyngrok import ngrok
from dotenv import load_dotenv
from wordcloud import WordCloud
from stop_words import get_stop_words
from threading import Condition, Thread
from catboost_endpoint import CatBoostWrapper

from BERT_endpoint import BERTClass, BertTokenizer, MODEL_NAME, torch

device = 'cuda' if cuda.is_available() else 'cpu'
load_dotenv()
ngrok_key = os.getenv("ngrok_api_key")
ip = os.getenv("ip_local")
bert_path=os.getenv("bert_path")
cb_object_path=os.getenv("cb_object_path")
cb_relevant_path=os.getenv("cb_relevant_path")
ngrok.set_auth_token(ngrok_key)
public_url_stream = ngrok.connect("5000", "http")

print(f"http://{ip}:5000 -> {public_url_stream}")


class MainApplication:
    def __init__(self):
        self.bert_sentiment = BERTClass()
        checkpoint = torch.load(bert_path)
        self.bert_sentiment.load_state_dict(checkpoint)
        self.bert_sentiment.eval()
        self.tokenizer = BertTokenizer.from_pretrained(MODEL_NAME)
        self.bert_sentiment.to(device)
        self.object_catboost = CatBoostWrapper(cb_object_path)
        self.relevant_catboost = CatBoostWrapper(cb_relevant_path)

    def compute_sentiment(self, text):
        inputs = self.tokenizer.encode_plus(
            text,
            None,
            add_special_tokens=True,
            max_length=200,
            pad_to_max_length=True,
            return_token_type_ids=True
        )
        ids = torch.tensor([inputs['input_ids']]).to(device)
        mask = torch.tensor([inputs['attention_mask']]).to(device)
        token_type_ids = torch.tensor([inputs["token_type_ids"]]).to(device)
        outputs = self.bert_sentiment(ids, mask, token_type_ids)
        outputs = torch.sigmoid(outputs).cpu().detach().numpy().tolist()
        return outputs[0].index(max(outputs[0]))

    def compute_object(self, text):
        outputs = self.object_catboost.compute_metric(text)
        return outputs

    def compute_relevant(self, text):
        outputs = self.relevant_catboost.compute_metric(text)
        return outputs

    def test_compute_all_metrics(self, data_frame):
        sentiment = data_frame.apply(lambda x: self.compute_sentiment(x))
        object_label = data_frame.apply(lambda x: self.compute_object(x)[0])
        relevant = data_frame.apply(lambda x: self.compute_relevant(x))
        return sentiment, object_label, relevant


def run_application():
    if __name__ == '__main__':
        threading.Thread(target=lambda: app.run(debug=False)).start()


application = MainApplication()
# testing.testing("train_data.csv", application)


@app.route('/compute_metrics', methods=['POST'])
def compute_metrics():
    answers = request.json['answers']
    if answers is None or not len(answers) == 4:
        return jsonify({"result": "error", "error_str": "error in data format"})

    sentiment_weighted = application.compute_sentiment(" ".join(answers))
    relevant_weighted = application.compute_relevant(" ".join(answers))
    object_label_weighted = application.compute_object(" ".join(answers))[0]
    return jsonify({"is_positive": True if round(sentiment_weighted) == 1 else False,
                    "object": round(object_label_weighted),
                    "is_relevant": True if round(relevant_weighted) == 1 else False})


@app.route('/compute_word_cloud', methods=['POST'])
def compute_word_cloud():
    reviews = request.json['reviews']
    if reviews is None or len(reviews) == 0:
        return jsonify({"result": "error", "error_str": "error in data format"})
    reviews = [" ".join(x) for x in reviews]

    STOPWORDS_RU = get_stop_words('russian')
    wordcloud = WordCloud(width=800,
                          height=400,
                          random_state=1,
                          background_color='black',
                          margin=20,
                          colormap='Pastel1',
                          collocations=False,
                          stopwords=STOPWORDS_RU).generate(" ".join(reviews))

    buffer = io.BytesIO()
    wordcloud.to_image().save(buffer, 'png')
    b64 = base64.b64encode(buffer.getvalue())
    words = [k for k in wordcloud.words_.keys()]
    return jsonify({"raw_image": b64.decode("utf-8"), "words_list": words})


run_application()
