
FROM python:3.7

WORKDIR /app

COPY requirements.txt /app/


RUN pip3 install --no-cache-dir -r requirements.txt

COPY . /app
EXPOSE 5000
CMD ["python3", "inference.py"]