import transformers
import torch
from transformers import BertTokenizer, BertModel, BertConfig

MODEL_NAME = "sismetanin/sbert-ru-sentiment-rusentiment"


class BERTClass(torch.nn.Module):
    def __init__(self):
        super(BERTClass, self).__init__()
        id2label = {
            0: "NEGATIVE",
            1: "POSITIVE",

        }
        label2id = {
            "NEGATIVE": 0,
            "POSITIVE": 1,

        }
        self.l1 = transformers.BertModel.from_pretrained(MODEL_NAME, torchscript=True, id2label=id2label,
                                                         label2id=label2id)

        # for param in self.l1.parameters():
        #    param.requires_grad = False

        self.l2 = torch.nn.Dropout(0.3)
        self.l3 = torch.nn.Linear(1024, 2)

    def forward(self, ids, mask, token_type_ids):
        _, output_1 = self.l1(ids, attention_mask=mask, token_type_ids=token_type_ids, return_dict=False)
        output_2 = self.l2(output_1)
        output = self.l3(output_2)
        return output
